'use strict';
const xml2js = require('xml2js');

async function sendJSONUpstream(bodyContent, srcReq) {
  if (!bodyContent || !bodyContent.length) {
    return '';
  }
  if (srcReq.get('Content-Type') === 'application/json') {
    return bodyContent
  } else {
    const parser = new xml2js.Parser(/* options */);
    const result = await parser.parseStringPromise(bodyContent.toString('utf8') +'');
    return result;
  }
}

function sniffXML(proxyRes, proxyResData, userReq, userRes) {
  if (userReq.accepts('application/json') !== false || !proxyResData.length) {
    return proxyResData;
  } else {        
    const builder = new xml2js.Builder({rootName: 'Data'});
    const xml = builder.buildObject(JSON.parse(proxyResData.toString('utf8')));
    userRes.set('Content-Type','application/xml; charset=utf-8');
    return xml;
  }
}

function modifyContentHeaders(proxyReqOpts, srcReq) {
  proxyReqOpts.headers['Content-Type'] = 'application/json; charset=utf-8';
  return proxyReqOpts;
}

module.exports = {
  xmlToJSON: {
    userResDecorator: sniffXML,
    proxyReqBodyDecorator: sendJSONUpstream,
    proxyReqOptDecorator: modifyContentHeaders
  }
};