'use strict';
function requireAuth(req, res, next) {
  if (!req.get('Authorization')) {
    return res.status(401).send('No thank you');
  }
  return next();
}

module.exports = {
  requireAuth
};