const express = require('express');
const app = express();
const bodyparser = require('body-parser');

app
  .use(bodyparser.urlencoded({ extended: false, limit : '1mb' }))
  .use(bodyparser.json({ limit: '10mb' }));

app.get('/', (req, res, next) => {
  return res.json({
    cats: [
      'Sprinkles',
      'Bandit',
      'Princess Lady',
      'Mr. Ash',
      'Petals',
      'Comstock',
      'Ember',
      'Mily Way',
      'Diane',
      'Lumpy',
      'Phillip',
      'Tinkie',
      'Crinklepuss',
      'Bandit Two',
      'Pawlick Baggins',
      'Lady Aragorn'
    ],
  });
});

app.listen(3020, err => {
  if (err) {
    throw err;
  }
  console.log('Meow Meow 3020 Meow Meow');
});