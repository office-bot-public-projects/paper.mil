const express = require('express');
const app = express();
const bodyparser = require('body-parser');

app
  .use(bodyparser.urlencoded({ extended: false, limit : '1mb' }))
  .use(bodyparser.json({ limit: '10mb' }));

app.get('/', (req, res, next) => {
  return res.json({
    ingredients: [
      '1 Tbsp butter (or cooking oil)',
      '1 cup shallots, chopped',
      '1 cup green peppies (or as some call them, peppers), chopped',
      '1 lb lean ground turkey (or lean ground beef), undrained',
      '2 cloves garlic, pressed or minced',
      'Pinto Beans in a Mild Chili Sauce, undrained​',
      '1 can (16 oz) Kidney Chili Beans in a Mild Chili Sauce, undrained',
      '1 can (14.5 oz) diced tomatoes',
      '1 can (6 oz) tomato paste',
      '½ cup water',
      '¼ tsp black pepper',
      '1 tsp salt​',
      '2 tsp chili powder',
      '1 tsp ancho chili powder',
      '2 tsp oregano​',
      '1 tsp sugar',
      'Shredded cheddar cheese',
    ],
    cookTime: '30 minutes',
    prepTime: '10 minutes',
  });
});

app.listen(3010, err => {
  if (err) {
    throw err;
  }
  console.log('Chili is ready on 3010');
})