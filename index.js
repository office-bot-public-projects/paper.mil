const proxy = require('express-http-proxy');
const contentService = require('./lib/xml-json.service');
const authService = require('./lib/auth.service');

var app = require('express')();
 
app.use('/kevin/chili/recipe', authService.requireAuth, proxy('http://localhost:3010', contentService.xmlToJSON));
app.use('/angela/cats/names', authService.requireAuth, proxy('http://localhost:3020', contentService.xmlToJSON));

app.listen(5000, err => {
  if (err) {
    throw err;
  }
  console.log(`Proxy ready on 5000`);
});